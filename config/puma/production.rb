#require 'puma/daemon'

app_dir = "/home/saji/Software/shortxxxvid" # Update me with your root app path

#bind  "tcp://0.0.0.0:9292"
port 9292
pidfile "#{app_dir}/puma.pid"
state_path "#{app_dir}/puma.state"
directory "#{app_dir}/"

stdout_redirect "#{app_dir}/log/puma.stdout.log", "#{app_dir}/log/puma.stderr.log", true

workers 2
threads 1,5

activate_control_app

prune_bundler
#daemonize
