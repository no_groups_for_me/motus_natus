require 'video_info'

VideoInfo.provider_api_keys = { youtube: 'AIzaSyDy5241-R9qSRv2YmcXKnT9UxXXZaHmbyE'}

def inspect_video_attributes(url)
  video = VideoInfo.new(url)
  return false unless video.available?
  return [video.title, video.date, video.provider, video.thumbnail_small,
         video.thumbnail_medium,video.thumbnail_large]
end
#video = VideoInfo.new("http://fast.wistia.com/embed/medias/pxonqr42is")
# video.available?       => true
# video.video_id         => 'x7lni3'
# video.provider         => 'Dailymotion'
# video.title            => 'Mario Kart (Rémi Gaillard)'
# video.description      => 'Super Rémi Kart '
# video.duration         => 136 (in seconds)
# video.date             => Mon Mar 03 16:29:31 UTC 2008
# video.thumbnail_small  => 'http://s2.dmcdn.net/BgWxI/x60-kbf.jpg'
# video.thumbnail_medium => 'http://s2.dmcdn.net/BgWxI/x240-b83.jpg'
# video.thumbnail_large  => 'http://s2.dmcdn.net/BgWxI/x720-YcV.jpg'
# video.embed_url        => 'http://www.dailymotion.com/embed/video/x7lni3'
# video.embed_code       => "<iframe src='//www.dailymotion.com/embed/video/x7lni3' frameborder='0' allowfullscreen='allowfullscreen'></iframe>"

