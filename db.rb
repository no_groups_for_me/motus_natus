require 'sequel'

DB = Sequel.connect('sqlite://shortxxxvids.db') # requires sqlite3
Model = Class.new(Sequel::Model)
Model.db = DB
Model.plugin :forme


DB.create_table?(:videos) do
  primary_key :id
  String :title,         :null=>false
  String :url,           :null=>false, :unique=>true
  String :provider,      :null=>false
  String :url_thumb_sml, :null=>true
  String :url_thumb_med, :null=>true
  String :url_thumb_lrg, :null=>true
  Date   :date_added,    :null=>false
  Date   :date_shot,     :null=>true
  String :image_data
  # video.image_url stores location of video
end

# remove this
DB.create_table?(:categories) do
  primary_key :id
  String :name, :null=>false
  foreign_key :video_id, :videos, :null=>false
end

#remove this
DB.create_table?(:tags) do
  primary_key :id
  String :name, :null=>false, :unique=>false
  foreign_key :category_id, :categories, :null=>false
end

# the_list table used only when creating new labels
#             or when accessing it
#  the entries in this list are never modified
DB.create_table?(:the_list) do
  primary_key :id
  String :name, :null=>false, :unique=>true
end

# when modifying name, consult the_list to get new cat_id
DB.create_table?(:the_categories) do
  primary_key :id
  Integer :cat_id, :unique=>true # id taken from the_list
  String :name # name of category
end

# when modifying name, consult the_list to get new sub_cat_id
DB.create_table?(:the_sub_categories) do
  primary_key :id
  String :name
  Integer :cat_id  # id taken from the_list
  Integer :sub_cat_id # id taken from the_list
end
