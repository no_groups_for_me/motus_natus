class TheList < Sequel::Model(:the_list)
  plugin :validation_helpers
  def validate
    super
    validates_unique :name
  end
end

class TheCategories < Sequel::Model(:the_categories)
  def subcategories
    TheSubCategories.where(cat_id: self.cat_id)
  end
end

class TheSubCategories < Sequel::Model(:the_sub_categories)
end
