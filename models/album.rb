class Category < Model
  many_to_one :video
  one_to_many :tags
end

class Tag < Model
  many_to_one :category
  many_to_one :video
end

class Protest
  def self.tags
    ["Rant", "Mass protest", "Police violence", "Business protest", "Covid divide"]
  end
end
class Science
  def self.tags
    ["PCR", "Masks", "Treatments", "Hospitals", "Immunity","Misattribution of deaths","Germ theory"]
  end
end
class Lockdowns
  def self.tags
    ["Children", "Quarantine centers", "Business", "Mental health", "Social Health"]
  end
end
class Culture
  def self.tags
    ["Never again", "Humour"]
  end
end
