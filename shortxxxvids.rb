require "roda"
require "forme"
require "tilt/erubi"
require './get_video_info.rb'
require_relative 'models.rb'
require 'yaml'
require 'pp'

def fill_video_info(url,vid_obj)
  res=inspect_video_attributes(url)
  vid_obj.url = url
  vid_obj.date_added=Date.today
  return vid_obj if res==false
  vid_obj.title=res[0] if res[0]
  date=res[1]
  vid_obj.date_shot=Date.new(date.year,date.month,date.day) if date
  vid_obj.provider=res[2] if res[2]
  vid_obj.url_thumb_sml=res[3] if res[3]
  vid_obj.url_thumb_med=res[4] if res[4]
  vid_obj.url_thumb_lrg=res[5] if res[5]
  return vid_obj
end

def add_to_list(name)
  res = TheList.find(name: name)
  # if id is nil, TheList does not contain the cat
    # if so, save to TheList, then save to TheCategories
  # find only finds 
  if res.nil?  
    TheList.new(name: name).save 
    lst_id = TheList.find(name: name).id
  else
    lst_id = res.id
  end
  lst_id
end

def add_to_category(cat)
  cat.strip!         # remove leading/trailing spaces
  cat.squeeze!(" ")  # only one space between words
  # first save to the_list
  lst_id = add_to_list(cat)

  # now save to the_categories
  cat_id = TheCategories.find(name: cat)
  if cat_id
    res=cat
  else
    TheCategories.new(name: cat, cat_id: lst_id).save
  end
  res
end

def delete_or_rename(cat,str,new_cat=nil)
  # collect all entries associated to this category
  # delete category 
  if str=="delete"
    cat.delete
  end
  # we will be associating entries to this
  # new category
  if str=="rename"
    # first add to list, if not in list
    lst_id = add_to_list(new_cat)
    cat.name=new_cat
    cat.cat_id=lst_id
    cat.save
  end
end

def add_to_sub_category(cat_id,sub_cat)
  sub_cat.strip!         # remove leading/trailing spaces
  sub_cat.squeeze!(" ")  # only one space between words
  # first save to the_list
  lst_id = add_to_list(sub_cat)

  # now save to the_categories
  sub_cat_id = TheSubCategories.find(name: sub_cat)
  if sub_cat_id
    res=sub_cat
  else
    TheSubCategories.new(name: sub_cat, cat_id: cat_id, sub_cat_id: lst_id).save
  end
  res
end

class ShortXXXVids < Roda
  plugin :forme
  plugin :public
  plugin :exception_page
  plugin :error_handler do |e|
    next exception_page(e)
    #File.read('public/500.html')
  end
  plugin :render#, escape: true
  plugin :sessions, secret: ENV.delete('APP_SESSION_SECRET')
  plugin :route_csrf
  plugin :flash
  plugin :typecast_params

#  Forme.register_config(:mine, :base=>:default, :labeler=>:explicit, :wrapper=>:div)
#  Forme.default_config = :mine

  route do |r|
     check_csrf!
    
    #puts "#{r}"

    r.on "roda" do
      r.public
      r.on "videos" do

        r.get do
          r.is "new" do
            @video=Video.new
            view "videos/new"
          end
          r.is Integer,String do |id,str|
            if (["edit","categorize","show"].any? {|s| s==str})
              @video = Video[id]
              view "videos/#{str}"
            end
          end
          r.is do
            next unless @videos=Video.all
            view "videos/index"
          end
        end #get

        r.post do
          r.is "fill" do
            @video=fill_video_info(r.params["url_scraper"].chomp,Video.new)
            view "videos/new"
          end
          r.is "create" do
            video = Video.new(r.params["video"])
            video.save
            @video = Video.last
            view "videos/categorize"
          end
          r.is "associate" do
            "<h1> Not yet associated </h1> <cite>Coming soon!</cite>"
            "#{r.params}"
          end
          r.is Integer, "update" do |id|
            @video=Video[id]
            @video.update(r.params["video"])
            r.redirect "/roda/videos"
          end
        end #post

      end #end_of "videos"

      r.on "categories" do
        r.get do
          r.is "new" do
            view "categories/new"
          end
          r.is Integer, String do |id,str|
            if (["delete","rename","subcategories"].any? {|s| s==str})
              @category = TheCategories[id]
              view "categories/#{str}/index"
            end
          end
          r.is do
            next unless @categories=TheCategories.all
            view "categories/index"
          end
        end #get
    
        r.post do
          r.is "create" do
            res=[]
            (1..10).to_a.each do |i|
              cat=typecast_params.nonempty_str("cats#{i}")
              if cat
                res<<add_to_category(cat)
              end
            end
            res.compact! # remove nil values
            flash['error']="categories #{res.join(",")} already taken" unless res.empty?
            r.redirect "/roda/categories"
          end
          r.is Integer, "subcategories","create" do |id|
            "hey #{id} #{r.params}"
            @category = TheCategories[id]
            cat_id = @category.cat_id
            res=[]
            (1..10).to_a.each do |i|
              sub_cat=typecast_params.nonempty_str("sub_cats#{i}")
              if sub_cat
                res<<add_to_sub_category(cat_id,sub_cat)
              end
            end
            view "categories/subcategories/index"
          end
          r.is Integer, String do |id,str|
            if (["delete","rename"].any? {|s| s==str})
              @category = TheCategories[id]
              new_cat = typecast_params.nonempty_str("name")
              delete_or_rename(@category,str,new_cat) unless new_cat.nil?
              r.redirect "/roda/categories"
            end
          end
        end #post
      end

    end # end of "roda"
  end # end of whole routing
end # end of class
