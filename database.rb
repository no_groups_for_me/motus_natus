require 'sequel'
require 'shrine'
require "shrine/storage/file_system"
 
Shrine.storages = { 
  cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"), # temporary 
  store: Shrine::Storage::FileSystem.new("public", prefix: "uploads"),       # permanent 
}
 
Shrine.plugin :sequel # or :activerecord 
Shrine.plugin :cached_attachment_data # for retaining the cached file across form redisplays 
Shrine.plugin :restore_cached_data # re-extract metadata when attaching a cached file 
Shrine.plugin :rack_file # for non-Rails apps 
Shrine.plugin :determine_mime_type

#DB = Sequel.sqlite # keep it just in memory
DB = Sequel.connect('sqlite://shortxxxvids.db') # requires sqlite3


DB.create_table?(:videos) do
  primary_key :id
  String :title,         :null=>false
  String :url,           :null=>false, :unique=>true
  String :provider,      :null=>false
  String :url_thumb_sml, :null=>true
  String :url_thumb_med, :null=>true
  String :url_thumb_lrg, :null=>true
  Date   :date_added,    :null=>false
  Date   :date_shot,     :null=>true
  String :image_data
  # video.image_url stores location of video
end

DB.create_table?(:categories) do
  primary_key :id
  String :name, :null=>false
  foreign_key :video_id, :videos, :null=>false
end

DB.create_table?(:tags) do
  primary_key :id
  String :name, :null=>false, :unique=>false
  foreign_key :category_id, :categories, :null=>false
end

Model = Class.new(Sequel::Model)
Model.db = DB
Model.plugin :forme
#Model.plugin :nested_attributes

class ImageUploader < Shrine
end

class Video < Model
  one_to_many :categories
  one_to_many :tags
  include ImageUploader::Attachment(:image) # adds an `image` virtual attribute 
end

class Category < Model
  many_to_one :video
  one_to_many :tags
end


class Tag < Model
  many_to_one :category
  many_to_one :video
end

class Protest
  def self.tags
    ["Rant", "Mass protest", "Police violence", "Business protest", "Covid divide"]
  end
end
class Science
  def self.tags
    ["PCR", "Masks", "Treatments", "Hospitals", "Immunity","Misattribution of deaths","Germ theory"]
  end
end
class Lockdowns
  def self.tags
    ["Children", "Quarantine centers", "Business", "Mental health", "Social Health"]
  end
end
class Culture
  def self.tags
    ["Never again", "Humour"]
  end
end
